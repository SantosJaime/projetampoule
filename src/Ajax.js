const BASE_URL = 'http://192.168.1.62:8080/api/v1/';

const onOrOff = {
    true: 'on',
    false: 'off',
}

function status(mac, element){

    fetch(BASE_URL+ 'devices/' + mac,) 
        .then (resp => resp.json())
        .then (bulle => {
            element.innerText = onOrOff[bulle.on]
         })
    
}

function colorRandomizer(){
    let bulbColor = (0x1000000 + Math.random() * 0xffffff).toString(16).substr(1, 6);
    //console.log(bulbColor);
    return bulbColor;
}

function bulb_color(mac, color) { 

    fetch(BASE_URL+ 'devices/' + mac, 
        {
            method: 'POST',
            headers : {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                //ramp: 2000 ,
                color: color,
            })
        }
    )

}


function toggle(mac) {
    let add = mac;   
    let test = onOff(add);

    return fetch(BASE_URL+ 'devices/' + mac, 
        {
            method: 'POST',
            headers : {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                action: test,
                ramp: 0000 ,
            })
        }
    ) 

}
 
function onOff(mac){

    let etat = document.getElementById(mac).value;

    if ( etat === 'on' ){ 
        document.getElementById(mac).value = 'off';
        return 'off';
    }else{
        document.getElementById(mac).value = 'on';
        return 'on';
    }

}

fetch ('http://192.168.1.62:8080/api/v1/devices')
  .then(resp => resp.json())
  .then(body => {
      const bulbList = document.querySelector('#bulbs');

        for (let bulb of body) {
            let htmlBulb = document.createElement('li');
            htmlBulb.innerText = bulb.mac;
            

            let buttonOnOff = document.createElement('button');
                //buttonOnOff.innerText
                //buttonOnOff.type = 'button';
                setInterval (() => status (bulb.mac, buttonOnOff),1000);
                buttonOnOff.classList.add("boutonOnOff");
                buttonOnOff.id = bulb.mac;
                buttonOnOff.addEventListener('click', () => toggle(bulb.mac).then(resp => resp.json()).then(body => buttonOnOff.innerText = onOrOff[body.on]));

                //status (bulb.mac, buttonOnOff)
                
            let buttonColor = document.createElement('button');
                setInterval (() => bulb_color (bulb.mac, buttonColor),1000);
                buttonColor.innerText = 'color';
                buttonColor.classList.add("boutonColor");
                buttonColor.addEventListener('click', () => {
                    let couleur = colorRandomizer();
                    bulb_color(bulb.mac, "00"+couleur);
                    buttonColor.style.backgroundColor= "#"+couleur;
                });
                        
            bulbList.appendChild(htmlBulb);
            bulbList.appendChild(buttonOnOff);
            buttonOnOff.innerHTML = document.getElementById(bulb.mac).value;
            bulbList.appendChild(buttonColor);
        }

  });
